def count_common_assignments(filename):
    count = 0
    with open(filename, "r") as f:
        for row in f:
            assignment = get_intervals(row)
            if is_symmetric_containing_assignments(assignment):
                count += 1
    return count


def get_intervals(row):
    l = row.replace("\n", "").split(sep=',')
    intervals = []
    for i in l:
        intervals.append([int(j) for j in i.split("-")])
    return intervals

def is_containing_assignments(ass_a, ass_b):
    return ass_a[0] >= ass_b[0] and ass_a[1] <= ass_b[1]

def  is_symmetric_containing_assignments(assignment):
    return is_containing_assignments(assignment[0],assignment[1]) or is_containing_assignments(assignment[1],assignment[0])


def main():

    print(count_common_assignments("input.txt"))


if __name__ == "__main__":
    main()
