def get_common_items(filename):
    with open(filename, "r") as f:
        for row in f:
            rucksack = row[0:-1]
            size_of_compartment = int(len(rucksack) / 2)
            compartment1 = rucksack[0:size_of_compartment]
            compartment2 = rucksack[size_of_compartment:]
            common = set(list(compartment1)).intersection(set(list(compartment2)))
            yield next(iter(common))


def sum_priority(fn):
    sum_of_priorities = 0
    for i in get_common_items(fn):
        sum_of_priorities += calculate_priority(i)
    return sum_of_priorities


def calculate_priority(item):
    value = ord(item)
    if value >= 97:  # a..z
        return value - 97 + 1
    else:
        return value - 65 + 27


def get_badge(fn):
    with open(fn, "r") as f:
        while True:
            elf1 = set(list(f.readline()[:-1]))
            if elf1 == set():
                break
            elf2 = set(list(f.readline()[:-1]))
            if elf2 == set():
                break
            elf3 = set(list(f.readline()[:-1]))
            if elf3 == set():
                break
            yield next(iter(elf1.intersection(elf2.intersection(elf3))))


def sum_of_badges(fn):
    sum_of_badge_priority = 0
    for i in get_badge(fn):
        sum_of_badge_priority += calculate_priority(i)
    return sum_of_badge_priority


def main():
    print(sum_priority("input.txt"))

    print(sum_of_badges("input.txt"))


if __name__ == "__main__":
    main()
