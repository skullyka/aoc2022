def get_sums_from_file(filename):
    with open(filename, "r") as f:
        sum_of_calories = 0
        for row in f:
            if row != "\n":
                sum_of_calories += int(row)
            else:
                yield sum_of_calories
                sum_of_calories = 0


def main():
    sums = []
    for i in get_sums_from_file("input.txt"):
        sums.append(i)
    print(max(sums))
    sums.sort()
    print(sum(sums[-3:]))


if __name__ == "__main__":
    main()
