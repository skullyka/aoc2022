def get_score(filename):
    with open(filename, "r") as f:
        round_points = 0
        for row in f:
            my = row[2]
            opponent = row[0]
            round_points += process_my_choice(row[2])
            round_points += process_battle(row[0], row[2])
        return round_points


def process_choice(choice, base):
    return ord(choice) - ord(base) + 1


def process_my_choice(choice):
    return process_choice(choice, "X")


def process_battle(opponent, my):
    opponent_value = process_choice(opponent, "A")
    my_value = process_my_choice(my)
    winner_pairs = [(1, 3), (2, 1), (3, 2)]
    if my_value == opponent_value:
        return 3
    elif (my_value, opponent_value) in winner_pairs:
        return 6
    else:
        return 0


def get_other_score(filename):
    with open(filename, "r") as f:
        round_points = 0
        for row in f:
            round_points += process_state(row[2])
            round_points += process_sign(row[0], row[2])
        return round_points


def process_state(state):
    return (ord(state) - ord("X")) * 3


def process_sign(opponent, state):
    winners = {
        "A": 2,
        "B": 3,
        "C": 1,
    }
    draw = process_choice(opponent, "A")
    if state == "Y":
        return draw
    elif state == "Z":
        return winners[opponent]
    else:
        return next(iter({1, 2, 3} - {draw, winners[opponent]}))


def main():
    print(get_score("input.txt"))

    print(get_other_score("input.txt"))


if __name__ == "__main__":
    main()
